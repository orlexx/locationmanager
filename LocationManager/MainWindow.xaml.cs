﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Data;
using System.Collections.ObjectModel;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public partial class MainWindow : Window
    {
        double tabWidth = 0.0;
        double widthToActDay = 0.0;

        public MainWindow()
        {
            InitializeComponent();
            Style = (Style)FindResource(typeof(Window));
            List<DateTime> daysList = new List<DateTime>();
            for (int i = -31; i < 31; i++)
            {
                double dayShift = Convert.ToDouble(i);
                DateTime day_dt = DateTime.Now.AddDays(dayShift);
                daysList.Add(day_dt);
            }
            BookingSchedule_dg.Columns.Clear();
            buildTable(daysList);
            scheduleSV.ScrollToHorizontalOffset(widthToActDay);
            //for (int i = 0; i < BookingSchedule_dg.Items.Count - 1; i++)
            //{
            //    for (int j = 0; j < BookingSchedule_dg.Columns.Count - 1; j++)
            //    {
            //        DataGridCell cell = GetCell(i, j);
            //        TextBlock textBlock = cell.Content as TextBlock;
            //        textBlock.Text = i.ToString() + " " + j.ToString();
                    
            //    }
            //}
            BookingSchedule_dg.UpdateLayout();
            List<Location> locations = Location.getLocations();
            TextBox tb = new TextBox();
            tb.Text = "";
            tb.FontSize = 16;
            tb.Padding = new Thickness(3);
            locations_sp.Children.Add(tb);
            foreach (Location l in locations)
            {
                TextBox locationNr_tb = new TextBox();
                locationNr_tb.Name = "locationNr_tb_" + l.LocationNr;
                locationNr_tb.Text = l.LocationNr.ToString();
                locationNr_tb.FontSize = 16;
                locationNr_tb.Padding = new Thickness(3);
                locations_sp.Children.Add(locationNr_tb);
                BookingSchedule_dg.Items.Add(l);
                BookingSchedule_dg.RowHeight = locationNr_tb.ActualHeight + 29.27;
            }
            //fillSchedule();
        }
        
        void buildTable(List<DateTime> headers)
        {
            foreach (DateTime dt in headers)
            {
                DataGridTextColumn c = new DataGridTextColumn();
                string dateSt = dt.ToString(" dd.MM ");
                c.Header = dateSt;
                
                BookingSchedule_dg.Columns.Add(c);
                tabWidth += c.Width.Value + 45;
                string[] dayMonth = dateSt.Split('.');
                int day = Convert.ToInt32(dayMonth[0]);
                int month = Convert.ToInt32(dayMonth[1]);
                int year = DateTime.Now.Year;
                DateTime date = new DateTime(year, month, day, 1, 0, 0);
                #region STYLES
                Style todayStyle = new Style();
                todayStyle.Setters.Add(new Setter
                {
                    Property = Control.BackgroundProperty,
                    Value = new SolidColorBrush(Colors.LightGray)
                });
                todayStyle.Setters.Add(new Setter
                {
                    Property = Control.FontWeightProperty,
                    Value = FontWeights.Bold
                });

                Style sundayStyle = new Style();
                sundayStyle.Setters.Add(new Setter
                {
                    Property = Control.BackgroundProperty,
                    Value = new SolidColorBrush(Colors.Goldenrod)
                });

                Style saturdayStyle = new Style();
                saturdayStyle.Setters.Add(new Setter
                {
                    Property = Control.BackgroundProperty,
                    Value = new SolidColorBrush(Colors.Gold)
                });
                #endregion
                if (dt.ToString(" dd.MM ") == DateTime.Now.ToString(" dd.MM "))
                {                    
                    BookingSchedule_dg.Columns[BookingSchedule_dg.Columns.Count - 1].HeaderStyle = todayStyle;
                    widthToActDay = tabWidth;
                }
                else
                {
                    if (date.DayOfWeek == DayOfWeek.Sunday)
                        BookingSchedule_dg.Columns[BookingSchedule_dg.Columns.Count - 1].HeaderStyle = sundayStyle;
                    if (date.DayOfWeek == DayOfWeek.Saturday)
                        BookingSchedule_dg.Columns[BookingSchedule_dg.Columns.Count - 1].HeaderStyle = saturdayStyle;
                }                  
            }
        }

        //public DataGridCell GetCell(int row, int column)
        //{
        //    DataGridRow rowContainer = GetRow(row);

        //    if (rowContainer != null)
        //    {
        //        DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);

        //        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
        //        if (cell == null)
        //        {
        //            cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
        //        }
        //        return cell;
        //    }
        //    return null;
        //}

        

        //public static DataGridRow GetSelectedRow(this DataGrid grid)
        //{
        //    return (DataGridRow)grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem);
        //}
        //public static DataGridRow GetRow(this DataGrid grid, int index)
        //{
        //    DataGridRow row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
        //    if (row == null)
        //    {
        //        // May be virtualized, bring into view and try again.
        //        grid.UpdateLayout();
        //        grid.ScrollIntoView(grid.Items[index]);
        //        row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
        //    }
        //    return row;
        //}

        //public static DataGridCell GetCell(this DataGrid grid, DataGridRow row, int column)
        //{
        //    if (row != null)
        //    {
        //        DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

        //        if (presenter == null)
        //        {
        //            grid.ScrollIntoView(row, grid.Columns[column]);
        //            presenter = GetVisualChild<DataGridCellsPresenter>(row);
        //        }

        //        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
        //        return cell;
        //    }
        //    return null;
        //}

        

        //public static T GetVisualChild<T>(Visual parent) where T : Visual
        //{
        //    T child = default(T);
        //    int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
        //    for (int i = 0; i < numVisuals; i++)
        //    {
        //        Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
        //        child = v as T;
        //        if (child == null)
        //        {
        //            child = GetVisualChild<T>(v);
        //        }
        //        if (child != null)
        //        {
        //            break;
        //        }
        //    }
        //    return child;
        //}

        //private void fillSchedule()
        //{
        //    int a = 0;
        //    foreach (DataGridRow rowContainer in GetDataGridRows(BookingSchedule_dg))
        //    {
        //        if(rowContainer != null)
        //        {
        //            DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
        //        }
        //    }
        //}

        //public IEnumerable<DataGridRow> GetDataGridRows(DataGrid grid)
        //{
        //    var itemsSource = grid.ItemsSource as IEnumerable;
        //    if (null == itemsSource) yield return null;
        //    foreach (var item in itemsSource)
        //    {
        //        var row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
        //        if (null != row) yield return row;
        //    }
        //}



        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Alexey Orlov, 2018.");
        }
        
        private void NewUser_Click(object sender, RoutedEventArgs e)
        {
            User newUser = new User();
            AddNewUserWindow anuw = new AddNewUserWindow(newUser);
            anuw.Show();
        }
        
        private void AllUsers_Click(object sender, RoutedEventArgs e)
        {

            ObservableCollection<User> users = User.GetUsers();
            AllUsersWindow auw = new AllUsersWindow(users);
            auw.Show();
        }

        private void NewEvent_Click(object sender, RoutedEventArgs e)
        {            
            Event myEvent = new Event();
            EventDetailsWindow edw = new EventDetailsWindow(myEvent);
            edw.Show();
        }

        private void SearchEvent_Click(object sender, RoutedEventArgs e)
        {
            SearchEventWindow sew = new SearchEventWindow();
            sew.Show();
        }        

        private void ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (sender == locations_sv)
            {
                scheduleSV.ScrollToVerticalOffset(e.VerticalOffset);
            }
            else
            {
                locations_sv.ScrollToVerticalOffset(e.VerticalOffset);
            }
        }

        private void locations_sv_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}
