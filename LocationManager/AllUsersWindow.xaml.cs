﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public partial class AllUsersWindow : Window
    {
        ObservableCollection<User> users;
        List<string> userTypeList = new List<string>() { "student", "professor", "academic assistant" };
               
        public AllUsersWindow(ObservableCollection<User> users)
        {
            InitializeComponent();
            Glob.disableButton(delete_b);            
            search_tb_ToDefault();
            this.users = users;
            usersGrid.ItemsSource = users;
            userType_cb.ItemsSource = userTypeList;
        }

        private void search_tb_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search_tb.Text == "Search...")
            {
                search_tb.Text = "";
                search_tb.FontStyle = FontStyles.Normal;
                search_tb.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void search_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (search_tb.Text == "") 
                search_tb_ToDefault();
        }

        private void search_tb_ToDefault()
        {
            search_tb.Text = "Search...";
            search_tb.FontStyle = FontStyles.Italic;
            search_tb.Foreground = new SolidColorBrush(Colors.Gray);
        }

        private void search_tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(search_tb.Text.Length > 2 && search_tb.Text != "Search...")
            {
                ObservableCollection<User> foundUsers = User.GetUsers(inName: search_tb.Text);
                usersGrid.ItemsSource = null;
                usersGrid.ItemsSource = foundUsers;
            }
            else
            {
                usersGrid.ItemsSource = users;
            }
        }
        
        private void usersGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            User selectedUser = ((User)usersGrid.SelectedItem);
            if (Glob.tempUserType != null)
            {
                selectedUser.userType = Glob.tempUserType;
            }
            Glob.tempUserType = null;
            
            if (Regex.IsMatch(selectedUser.name.ToString(), @"^[\p{Lu}][\p{L}\x20\-]*('?)[\p{L}\x20\-]*[\p{Ll}]+$"))
            {
                if (User.updateUser(selectedUser))
                    MessageBox.Show(selectedUser.name + " has been succesfully updated.");
            }
            else
            {
                usersGrid_Update();
                MessageBox.Show("Wrong format.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }        
        private void usersGrid_Update()
        {
            usersGrid.ItemsSource = User.GetUsers();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox) {                
                var comboBox = sender as ComboBox;
                if (comboBox.SelectedItem != null)
                {
                    Glob.tempUserType = comboBox.SelectedItem.ToString();
                }                                
            }             
        }

        private void delete_b_Click(object sender, RoutedEventArgs e)
        {
            if (usersGrid.SelectedItem != null)
            {
                User selectedUser = ((User)usersGrid.SelectedItem);
                string query = "DELETE FROM t_users WHERE UserID = ?UserID";
                MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                command.Parameters.AddWithValue("?UserID", selectedUser.Uid);
                string message = "Do you want to delete " + selectedUser.name + "?";
                string caption = "Confirmation";
                MessageBoxResult result = MessageBox.Show(message, caption,
                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    DB_Connector.connection.Open();
                    command.ExecuteNonQuery();
                    DB_Connector.connection.Close();
                    usersGrid_Update();
                }                    
            }
        }

        private void usersGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            if (usersGrid.SelectedItem != null)
            {
                Glob.enableButton(delete_b);                
            }
            else
            {
                Glob.disableButton(delete_b);                
            }
        }        
    }
}
