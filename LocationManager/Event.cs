﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public class Event
    {
        public int EventID { get; set; }
        public string Date { get; set; }
        public DateTime EventStartDateTime { get; set; }
        public DateTime EventEndDateTime { get; set; }
        public User Instructor { get; set; }
        public Location EventLocation { get; set; }
        public string EventName { get; set; }
        public string Description { get; set; }

        public Event(int eventID, 
            DateTime eventStartDateTime, 
            DateTime eventEndDateTime,
            User instructor, 
            Location eventLocation, 
            string eventName,
            string description)
        {
            this.EventID = eventID;
            this.EventStartDateTime = eventStartDateTime;
            this.EventEndDateTime = eventEndDateTime;
            this.Instructor = instructor;
            this.EventLocation = eventLocation;
            this.EventName = eventName;
            this.Description = description;
            this.Date = EventStartDateTime.ToShortDateString();
        }

        public Event()
        {
            this.EventID = 0;
            this.EventStartDateTime = new DateTime();
            this.EventEndDateTime = new DateTime();
            this.Instructor = new User();
            this.EventLocation = new Location();
            this.EventName = "";
            this.Description = "";
            this.Date = "";
        }
    }
}
