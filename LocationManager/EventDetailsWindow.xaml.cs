﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public partial class EventDetailsWindow : Window
    {
        DateTime beginDateTime = new DateTime();
        DateTime endDateTime = new DateTime();
        User instructor = new User();
        Location location = new Location();
        Event thisEvent;
        List<User> enrolledUsers = new List<User>();

        public EventDetailsWindow(Event myEvent)
        {
            InitializeComponent();
            
            resetTimeTextBox(beginTime_tbx);
            resetTimeTextBox(endTime_tbx);
            thisEvent = myEvent;
            fillUsersDgWithUsers(myEvent.EventID);
            users_dg.SelectedItem = null;

            eventName_tbx.Text = thisEvent.EventName;
            eventId_tb.Text = thisEvent.EventID.ToString();
            if (eventId_tb.Text == "0")
            {
                eventId_tb.FontStyle = FontStyles.Italic;
                eventId_tb.Foreground = new SolidColorBrush(Colors.Gray);
            }
            instructor_tbx.Text = thisEvent.Instructor.name;
            if (thisEvent.EventStartDateTime != DateTime.MinValue)
            {
                date_dp.Text = thisEvent.EventStartDateTime.ToShortDateString();
                beginTime_tbx.Text = thisEvent.EventStartDateTime.ToShortTimeString();
                endTime_tbx.Text = thisEvent.EventEndDateTime.ToShortTimeString();
                beginTime_tbx.FontStyle = FontStyles.Normal;
                beginTime_tbx.Foreground = new SolidColorBrush(Colors.Black);
                endTime_tbx.FontStyle = FontStyles.Normal;
                endTime_tbx.Foreground = new SolidColorBrush(Colors.Black);
            }
            Description_tbx.Text = thisEvent.Description;
        }

        #region TIMETEXTBOXES_HANDLING
        private void beginTime_tbx_GotFocus(object sender, RoutedEventArgs e)
        {
            activateTextBox(beginTime_tbx);
        }
        private void endTime_tbx_GotFocus(object sender, RoutedEventArgs e)
        {
            activateTextBox(endTime_tbx);
        }

        private void beginTime_tbx_LostFocus(object sender, RoutedEventArgs e)
        {
            timeTbx_lostFocus(beginTime_tbx);         
        }
        private void endTime_tbx_LostFocus(object sender, RoutedEventArgs e)
        {
            timeTbx_lostFocus(endTime_tbx);
        }      

        private void beginTime_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            timeTbx_textChanged(beginTime_tbx);
        }   
        private void endTime_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            timeTbx_textChanged(endTime_tbx);            
        }

        private void beginTime_tbx_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            timeTbx_KeyUp(textBox, e);
        }
        private void endTime_tbx_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            timeTbx_KeyUp(textBox, e);
        }

        private void beginTime_tbx_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsTextAllowed(e.Text);
        }
        private void endTime_tbx_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsTextAllowed(e.Text);
        }

        private void timeTbx_textChanged(TextBox textBox)
        {
            int caretIndex;
            if (textBox.CaretIndex != textBox.Text.Length)
                caretIndex = textBox.CaretIndex;
            else caretIndex = -1;
            if (textBox.Text.Length > 2 && textBox.Text[2] != ':')
            {
                if (textBox.Text.Contains(':') == true)
                {
                    textBox.Text = textBox.Text.Replace(":", "");                    
                }  
                else
                    textBox.Text = textBox.Text.Insert(2, ":");
                         
            }
            if (caretIndex == -1)
                textBox.CaretIndex = textBox.Text.Length;
            else
                textBox.CaretIndex = caretIndex;    
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+"); 
            return regex.IsMatch(text);
        }

        private void showTimeTbxError(TextBox textBox)
        {
            string message = "Check time format, please.";
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            resetTimeTextBox(textBox);
        }

        private void timeTbx_KeyUp(TextBox textBox, KeyEventArgs e)
        {
            if (textBox.Text.Length == 2 && e.Key != Key.Back)
            {
                textBox.Text += ":";
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void timeTbx_lostFocus(TextBox textBox)
        {
            if (textBox.Text.Length == 5)
            {
                string[] timeElements = textBox.Text.Split(':');
                int hour = -1;
                int minute = -1;
                Int32.TryParse(timeElements[0], out hour);
                Int32.TryParse(timeElements[1], out minute);
                if (minute >= 60 || minute < 0 ||
                    hour >= 24 || hour < 0)
                {
                    showTimeTbxError(textBox);
                }
                else
                {
                    if (textBox.Name == "beginTime_tbx")
                    {
                        int year = beginDateTime.Year;
                        int month = beginDateTime.Month;
                        int day = beginDateTime.Day;
                        beginDateTime = new DateTime(year, month, day, hour, minute, 0);
                    }
                    if (textBox.Name == "endTime_tbx")
                    {
                        int year = beginDateTime.Year;
                        int month = beginDateTime.Month;
                        int day = beginDateTime.Day;
                        endDateTime = new DateTime(year, month, day, hour, minute, 0);
                    }
                }
            }
            else
            {
                showTimeTbxError(textBox);
            }
        }

        private void resetTimeTextBox(TextBox textBox)
        {
            textBox.Text = "HH:mm";
            textBox.Foreground = new SolidColorBrush(Colors.Gray);
            textBox.FontStyle = FontStyles.Italic;
        }
        #endregion

        private void activateTextBox(TextBox textBox)
        {
            if (textBox.Text == "HH:mm")
            {
                textBox.Foreground = new SolidColorBrush(Colors.Black);
                textBox.FontStyle = FontStyles.Normal;
                textBox.Text = "";
            }
        }

        private bool validateInput()
        {
            bool isEventNameValid = false;
            bool isInstructorValid = false;
            bool isDateValid = false;
            bool isTimeValid = false;
            bool isLocationValid = false;
            string query;
            bool isValid = false;

            List<string> resList = new List<string>();

            //Checking event name.
            if (eventName_tbx.Text != "")
                isEventNameValid = true;

            //Checking the instructor.
            isInstructorValid = User.userExists(instructor_tbx.Text);
            if (isInstructorValid == false)
            {
                MessageBoxResult result =  MessageBox.Show("Would you like  new user to be instructor?", 
                    "Question", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
                if (result == MessageBoxResult.Yes)
                {
                    if (Regex.IsMatch(instructor_tbx.Text, @"^[\p{Lu}][\p{L}\x20\-]*('?)[\p{L}\x20\-]*[\p{Ll}]+$"))
                    {
                        instructor.name = instructor_tbx.Text;
                        AddNewUserWindow anuw = new AddNewUserWindow(instructor, true);
                        if (anuw.ShowDialog() == true)
                        {
                            instructor.name = anuw.userName;
                            instructor.userType = anuw.userType;
                            instructor.Uid = anuw.userId;
                            instructor_tbx.Text = instructor.name;
                        }
                        if (instructor_tbx.Text != "")
                            isInstructorValid = true;
                    }
                }
            }
            else
            {
                instructor = User.getSingleUser(instructor_tbx.Text);
            }
               
            //Checking the date
            if (date_dp.SelectedDate != null)
                isDateValid = true;

            // Checking time
            if (beginTime_tbx.Text != "HH:mm" && endTime_tbx.Text != "HH:mm")
                isTimeValid = true;

            // Checking location
            query = "SELECT * FROM t_locations WHERE locNr = ?locNr;";           
            using (DB_Connector.connection)
            {
                MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                command.Parameters.AddWithValue("?locNr", Convert.ToInt32(location_tbx.Text));
                DB_Connector.connection.Open();       
                MySqlDataReader reader = command.ExecuteReader();            
                if (reader.HasRows == true)
                {
                    isLocationValid = true;
                    while (reader.Read())
                    {
                        location.LocationNr = reader.GetInt32("locNr");
                        location.LocationType = reader.GetString("locType");
                    }
                }
                reader.Close();
            } //end using connection

            // checking all criteria
            if (isEventNameValid && isInstructorValid && isDateValid &&
                isTimeValid && isLocationValid)
            {
                isValid = true;
            }
            return isValid;
        }

        #region BUTTONS

        private void Cancel_b_Click(object sender, RoutedEventArgs e)
        {
            if (eventName_tbx.Text != "" ||
                instructor_tbx.Text != "" ||
                date_dp.SelectedDate != null ||
                beginTime_tbx.Text != "HH:mm" ||
                endTime_tbx.Text != "HH:mm" ||
                location_tbx.Text != "" ||
                Description_tbx.Text != "")
            {
                string message = "All unsaved data may go lost. Are you sure?";
                string caption = "Question.";
                MessageBoxResult result = MessageBox.Show(message, caption, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }            
        }

        private void Clear_b_Click(object sender, RoutedEventArgs e)
        {
            if (eventName_tbx.Text != "" ||
                instructor_tbx.Text != "" ||
                date_dp.SelectedDate != null ||
                beginTime_tbx.Text != "HH:mm" ||
                endTime_tbx.Text != "HH:mm" ||
                location_tbx.Text != "" ||
                Description_tbx.Text != "")
            {
                string message = "Would you like to delete all entered data from this form?";
                string caption = "Question.";
                MessageBoxResult result = MessageBox.Show(message, caption, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    eventId_tb.Text = "";
                    eventName_tbx.Text = "";
                    instructor_tbx.Text = "";
                    date_dp.SelectedDate = null;
                    resetTimeTextBox(beginTime_tbx);
                    resetTimeTextBox(endTime_tbx);
                    location_tbx.Text = "";
                    Description_tbx.Text = "";
                }
            }
        }

        private void Save_b_Click(object sender, RoutedEventArgs e)
        {            
            if (isInPast())
                MessageBox.Show("You cannot change past events.");
            else
            {
                if (validateInput() == false)
                    MessageBox.Show("Please check your input.");
                else
                {                        
                    if (eventId_tb.Text != "0") // not new Event
                    {
                        updateOrInsert(false);  // isEventNew == false means 
                                                // (not new)
                                                // this event is already saved in the DB.
                        MessageBox.Show("Event successfully updated.");
                    }
                    else
                    {
                        bool eventInserted = updateOrInsert(true);   // isEventNew == true
                        thisEvent.EventID = Convert.ToInt32(eventId_tb.Text);                        
                        if(eventInserted)
                        {
                            enrollListOfUsers(enrolledUsers);
                            MessageBox.Show("Event successfully saved.");
                        }
                    }      
                }
            }
        }

        private void enroll_b_Click(object sender, RoutedEventArgs e)
        {
            if (isInPast())
                MessageBox.Show("You cannot change past events.");
            else
            {
                enroll();
                getEnrolledUsers();
                popUpClose();
            }
        }

        private void revoke_b_Click(object sender, RoutedEventArgs e)
        {
            if (users_dg.SelectedItem != null)
            {
                if (thisEvent.EventID == 0) // means Event is new
                {                                
                    enrolledUsers.RemoveAt(users_dg.SelectedIndex);
                    users_dg.ItemsSource = null;
                    users_dg.ItemsSource = enrolledUsers;
                }
                else
                {
                    if (isInPast() == false)
                    {                    
                        User user = (User)users_dg.SelectedItem;
                        string query = "DELETE FROM t_events_users WHERE UserID = ?UserID";
                        MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("UserID", user.Uid);
                        using (DB_Connector.connection)
                        {
                            DB_Connector.connection.Open();
                            command.ExecuteNonQuery();
                        }                    
                        getEnrolledUsers();
                               
                    }
                    else
                    {
                        MessageBox.Show("You cannot change past events.");
                    }

                }
            }
        }

        private void delete_btn_Click(object sender, RoutedEventArgs e)
        {
            if (isInPast())
                MessageBox.Show("You cannot delete past events.");
            else
            {
                if(thisEvent.EventID != 0)
                {
                    string query = "DELETE FROM t_events WHERE EventID = ?EventID";
                    MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                    command.Parameters.AddWithValue("?EventID", thisEvent.EventID);
                    MessageBoxResult result = MessageBox.Show(
                        "Do you want to delete this event?",
                        "Confirmation",
                        MessageBoxButton.YesNo, 
                        MessageBoxImage.Question);
                    if(result == MessageBoxResult.Yes)
                    {
                        using (DB_Connector.connection)
                        {
                            if (DB_Connector.connection.State != System.Data.ConnectionState.Open)
                                DB_Connector.connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                    MessageBox.Show("Event successfully deleted.");
                }
            }
        }

        #endregion

        private bool updateOrInsert(bool isEventNew)
        {
            bool result = false;
            string query;
            int year, month, day, startHour, startMin, endHour, endMin;
            string[] startTime = beginTime_tbx.Text.Split(':');
            string[] endTime = endTime_tbx.Text.Split(':');

            year = date_dp.SelectedDate.Value.Year;
            month = date_dp.SelectedDate.Value.Month;
            day = date_dp.SelectedDate.Value.Day;
            startHour = Convert.ToInt32(startTime[0]);
            startMin = Convert.ToInt32(startTime[1]);
            endHour = Convert.ToInt32(endTime[0]);
            endMin = Convert.ToInt32(endTime[1]);

            DateTime startDateTime = new DateTime(year, month, day, startHour, startMin, 0);
            DateTime endDateTime = new DateTime(year, month, day, endHour, endMin, 0);

            if (startDateTime >= endDateTime)
            {
                MessageBox.Show("No event can start later than it ends.");
                resetTimeTextBox(beginTime_tbx);
                resetTimeTextBox(endTime_tbx);
                return result;
            }
            else
            {
                if (isEventNew)
                {
                    query = "INSERT INTO t_events (start_datetime, end_datetime, " + 
                        "InstructorID, LocationID, Name, Description) VALUES (?start_datetime, " + 
                        "?end_datetime, ?InstructorID, ?LocationID, ?Name, ?Description); " +
                        "SELECT LAST_INSERT_ID();";
                }
                else
                {
                    query = "UPDATE t_events SET start_datetime = ?start_datetime, " + 
                        "end_datetime = ?end_datetime, InstructorID = ?InstructorID, " +
                        "LocationID = ?LocationID, Name = ?Name, Description = ?Description " +
                        "WHERE EventID = ?EventID;";

                }
                DB_Connector.getConnection();
                MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                command.Parameters.AddWithValue("?start_datetime", startDateTime);
                command.Parameters.AddWithValue("?end_datetime", endDateTime);
                command.Parameters.AddWithValue("?InstructorID", instructor.Uid);
                command.Parameters.AddWithValue("?LocationID", location.LocationNr);
                command.Parameters.AddWithValue("?Name", eventName_tbx.Text);
                command.Parameters.AddWithValue("?Description", Description_tbx.Text);
                command.Parameters.AddWithValue("?EventID", thisEvent.EventID.ToString());                
                if (isEventNew)
                {
                    if (isLocationFree(startDateTime, endDateTime))
                    {
                        DB_Connector.connection.Open();
                        eventId_tb.Text = command.ExecuteScalar().ToString(); 
                        DB_Connector.connection.Close();
                        result = true;
                    }
                    else
                    {
                        MessageBox.Show("Unfortunately the location isn't free at the requested time.");
                        result = false;
                    }                    
                }
                else
                {
                    DB_Connector.connection.Open();
                    command.ExecuteNonQuery();
                    DB_Connector.connection.Close();
                    result = true;
                }
                return result;
            }
        }

        private void enrollListOfUsers(List<User> list)
        {
            string query = "INSERT INTO t_events_users (EventID, UserID) VALUES" +
                    "(?EventID, ?UserID)";
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("EventID", thisEvent.EventID);
            foreach (User u in list)
            {
                if(command.Parameters.Contains("UserID") == false)
                {
                    command.Parameters.AddWithValue("UserID", u.Uid);
                }
                else
                {
                    command.Parameters["UserID"].Value = u.Uid;
                }
                
                using (DB_Connector.connection)
                {
                    if (DB_Connector.connection.State != System.Data.ConnectionState.Open)
                        DB_Connector.connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        private void fillUsersDgWithUsers(int inEventID)
        {
            users_dg.ItemsSource = User.GetUsers(inEventId: inEventID);
        }
        
        private bool isLocationFree(DateTime start, DateTime end)
        {
            int eventsStarted;
            int eventsEnded;
            DateTime beginDay = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0);
                //first, check if the location is free at start time. To archieve this check, how many events start 
                // at the same day earlier...
            string query = "SELECT count(*) FROM t_events WHERE start_datetime BETWEEN ?beginDay AND ?start" +
                " AND LocationID = ?location";
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?beginDay", beginDay);
            command.Parameters.AddWithValue("?start", start);
            command.Parameters.AddWithValue("?location", location_tbx.Text);
            using (DB_Connector.connection)
            {
                if(DB_Connector.connection.State != System.Data.ConnectionState.Open)
                    DB_Connector.connection.Open();
                eventsStarted = Convert.ToInt32(command.ExecuteScalar());
            }
                //now we know the amount of events starting at the same day, but earlier. Now we have to figure out how many events 
                //end at the same day, but earlier.
            query = "SELECT count(*) FROM t_events WHERE (end_datetime BETWEEN ?beginDay AND ?start)" +
                " AND LocationID = ?location";
            command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?beginDay", beginDay);
            command.Parameters.AddWithValue("?start", start);
            command.Parameters.AddWithValue("?location", location_tbx.Text);
            using (DB_Connector.connection)
            {
                DB_Connector.connection.Open();
                eventsEnded = Convert.ToInt32(command.ExecuteScalar());
            }
                //if there are the same amount of events that start and end before our event, it means, that
                //at the start time of our event our location is free, if that is not the case, we have to return false.
            if (eventsStarted > eventsEnded)
                return false;
                // Now we check if there are events that start at the same time or during our event.
            query = "SELECT count(*) FROM t_events WHERE (start_datetime BETWEEN ?start AND ?end)" +
                " AND LocationID = ?location";
            command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?start", start);
            command.Parameters.AddWithValue("?end", end);
            command.Parameters.AddWithValue("?location", location_tbx.Text);
            int eventsCrossing;
            using (DB_Connector.connection)
            {
                DB_Connector.connection.Open();
                eventsCrossing = Convert.ToInt32(command.ExecuteScalar());
            }
                //if there are such events, we return false, otherwise return true.
            if (eventsCrossing > 0)
                return false;
            else
                return true;
        }       

        private bool isInPast()
        {
            if (thisEvent.EventStartDateTime < DateTime.Now &&
                thisEvent.EventStartDateTime != DateTime.MinValue)
                return true;
            else
                return false;
        }
        
        private void newParticipant_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(isInPast() == false)
            {
                if (newParticipant_tbx.Text.Length > 2)
                {
                    List<string> userStrings;
                    if(eventId_tb.Text != "0")
                    {
                        userStrings = EnrollmentListExistingEvent();
                    }
                    else
                    {
                        userStrings = EnrollmentListNewEvent();
                    }
                    suggest_lb.ItemsSource = userStrings;
                    popUp.IsOpen = true;
                    suggest_lb.Visibility = Visibility.Visible;
                }
                else
                {
                    popUpClose();
                }
            }
            
        }

        private List <string> EnrollmentListExistingEvent()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM t_users u WHERE u.UserID NOT IN " +
                    "(SELECT UserID FROM t_events_users WHERE EventID = ?EventID) AND " +
                    "u.Name LIKE CONCAT('%', ?Name, '%')");
            command.Parameters.AddWithValue("?EventID", thisEvent.EventID);
            command.Parameters.AddWithValue("?Name", newParticipant_tbx.Text);
            ObservableCollection<User> users = DB_Connector.GetUsers(command);
            List<string> userStrings = new List<string>();
            foreach (User u in users)
            {
                userStrings.Add(u.name);
            }
            return userStrings;
        }

        private List<string> EnrollmentListNewEvent()
        {
            List<string> result = new List<string>();
            ObservableCollection<User> users = User.GetUsers();
            foreach(User u in users.Reverse<User>())
            {
                foreach(User v in enrolledUsers)
                {
                    if (u.name == v.name)
                        users.Remove(u);
                }
            }
            foreach(User u in users)
            {
                if(Regex.IsMatch(u.name, newParticipant_tbx.Text, RegexOptions.IgnoreCase))
                {
                    result.Add(u.name);
                }
            }        
            return result;
        }

        private void popUpClose()
        {
            suggest_lb.ItemsSource = null;
            popUp.IsOpen = false;
            suggest_lb.Visibility = Visibility.Collapsed;
        }
        
        private void newParticipant_tbx_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down && suggest_lb.Items.Count > 0)
            {
                Keyboard.ClearFocus();
                suggest_lb.SelectedIndex = 0;                
                var listBoxItem = (ListBoxItem)suggest_lb.ItemContainerGenerator.ContainerFromItem(suggest_lb.Items[0]);
                FocusManager.SetIsFocusScope(listBoxItem, true);
                FocusManager.SetFocusedElement(listBoxItem, listBoxItem);
            }
        }
        private void enroll()
        {
            if (suggest_lb.SelectedItem != null)
            {
                User user = new User();
                MySqlCommand command = new MySqlCommand("", DB_Connector.connection);
                command.CommandText = "SELECT * FROM t_users WHERE Name = ?Name";
                command.Parameters.AddWithValue("?Name", suggest_lb.SelectedItem);
                    
                using (DB_Connector.connection)
                {
                    if (DB_Connector.connection.State != System.Data.ConnectionState.Open)
                        DB_Connector.connection.Open();
                    MySqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            user.Uid = Convert.ToInt32(reader["UserID"]);
                            user.name = reader["Name"].ToString();
                            user.userType = reader["UserType"].ToString();
                        }
                    }
                }

                if(thisEvent.EventID == 0) // means Event is new
                {
                    bool isInList = false;
                    if (enrolledUsers.Count > 0)
                    {
                        foreach(User u in enrolledUsers)
                        {
                            if (u.Uid == user.Uid)
                            {
                                isInList = true;
                                break;
                            }
                        }
                    }
                    
                    if(isInList == false)
                    {
                        enrolledUsers.Add(user);                        
                    }

                    users_dg.ItemsSource = null;
                    users_dg.ItemsSource = enrolledUsers;
                }
                
                else 
                {
                    command.CommandText = "INSERT INTO t_events_users (EventID, UserID) VALUES" +
                        "(?EventID, ?UserID)"; ;
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("?EventID", thisEvent.EventID.ToString());
                    command.Parameters.AddWithValue("?UserID", user.Uid);
                    using (DB_Connector.connection)
                    {
                        if (DB_Connector.connection.State != System.Data.ConnectionState.Open)
                            DB_Connector.connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
                
            }
        }
        private void getEnrolledUsers()
        {
            enrolledUsers.Clear();
            string query = "SELECT * FROM t_users AS u INNER JOIN t_events_users as eu WHERE eu.EventID = ?EventID " + 
                "AND eu.UserID = u. UserID;";                
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?EventID", thisEvent.EventID.ToString());
            using (DB_Connector.connection)
            {
                DB_Connector.connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                if(reader.HasRows)
                {
                    while (reader.Read())
                    {
                        User enUser = new User(
                            Convert.ToInt32(reader["UserID"].ToString()),
                            reader["Name"].ToString(),
                            reader["UserType"].ToString()
                            );
                        enrolledUsers.Add(enUser);
                    }
                    reader.Close();
                }
            }                
            users_dg.ItemsSource = null;
            users_dg.ItemsSource = enrolledUsers;
        }

        private void suggest_lb_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (isInPast())
                MessageBox.Show("You cannot change past events.");
            else
            {
                if(e.Key == Key.Enter && isInPast() == false)
                {
                    enroll();
                    if(thisEvent.EventID != 0)
                    {
                        getEnrolledUsers();
                    }                    
                    popUpClose();
                    newParticipant_tbx.Focus();
                    users_dg.ItemsSource = null;
                    users_dg.ItemsSource = enrolledUsers;
                }
            }
        }
    }
}