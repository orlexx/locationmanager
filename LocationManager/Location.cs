﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public class Location
    {
        public int LocationNr { get; set; }
        public string LocationType { get; set; }
        public List<Event> Events { get; set; }
        
        public Location(int locationNr, string locationType)
        {
            LocationNr = locationNr;
            LocationType = locationType;
            Events = new List<Event>();
        }
        public Location()
        {
            LocationNr = 0;
            LocationType = "";
            Events = new List<Event>(); 
        }
        public Location(int locationNr)
        {
            LocationNr = locationNr;
            LocationType = "";
            Events = new List<Event>();
        }

        public static List<Location> getLocations()
        {
            List<Location> locations = new List<Location>();
            MySqlConnection connection = DB_Connector.getConnection();
            string query = "SELECT * FROM t_locations";
            MySqlCommand command = new MySqlCommand(query, connection);
            connection.Open();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                int locationNr = (int)reader["locNr"];
                string locationType = reader["locType"].ToString();
                Location location = new Location(locationNr, locationType);
                locations.Add(location);                
            }

            connection.Close();
            return locations;
        }
    }
}
