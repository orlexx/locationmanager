﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace LocationManager
{
    public class User
    {
        public int Uid { get; set; }
        public string name { get; set; }
        public string userType { get; set; }

        public User(int uid, string name, string userType)
        {
            this.Uid = uid;
            this.name = name;
            this.userType = userType;
        }
        
        public User()
        {
            this.Uid = 0;
            this.name = "";
            this.userType = "";
        }
        public User(int uid)
        {
            this.Uid = uid;
            this.name = "";
            this.userType = "";
        }

        public User(string userName)
        {
            this.Uid = 0;
            this.name = userName;
            this.userType = "";
        }

        public static User getSingleUser(string inName)
        {
            MySqlCommand command = new MySqlCommand();
            command.CommandText = "SELECT * FROM t_users WHERE Name LIKE " +
                        "CONCAT('%', ?Name, '%');";
            command.Parameters.AddWithValue("?Name", inName);
            return DB_Connector.GetUsers(command)[0];

        }

        public static ObservableCollection<User> GetUsers(string inName = "", int inEventId = -1, int inUserId = -1)
        {
            ObservableCollection<User> users = new ObservableCollection<User>();
            MySqlCommand command = new MySqlCommand();
            if(inUserId == -1)
            {
                if(inName == "" && inEventId == -1)
                {              
                    command.CommandText = "SELECT * FROM t_users";
                    users = DB_Connector.GetUsers(command);
                }
                if (inName == "" && inEventId != -1)
                {
                    command.CommandText = "SELECT * FROM t_users AS u CROSS JOIN t_events_users AS eu " +
                        "WHERE u.UserID = eu.UserID AND eu.EventID = ?EventID;";
                    command.Parameters.AddWithValue("?EventID", inEventId);
                    users = DB_Connector.GetUsers(command);
                }
                if (inName != "" && inEventId == -1)
                {
                    command.CommandText = "SELECT * FROM t_users WHERE Name LIKE " +
                        "CONCAT('%', ?Name, '%');";
                    command.Parameters.AddWithValue("?Name", inName);
                    users = DB_Connector.GetUsers(command);           
                }       
                if (inName != "" && inEventId != -1)
                {
                    command.CommandText = "SELECT * FROM t_users AS u CROSS JOIN t_events_users AS eu " +
                        "WHERE u.UserID = eu.UserID AND u.Name LIKE CONCAT('%', ?Name, '%') AND " + 
                        "eu.EventID = ?EventID;";
                    command.Parameters.AddWithValue("?Name", inName);
                    command.Parameters.AddWithValue("?EventID", inEventId);
                    users = DB_Connector.GetUsers(command);
                }
            }
            else
            {
                command.CommandText = "SELECT * FROM t_users WHERE UserID = ?userID";
                command.Parameters.AddWithValue("?userID", inUserId);
                users = DB_Connector.GetUsers(command);
            }
            return users;
        }

        public static bool updateUser (User user)
        {
            bool updated;
            string query = "UPDATE t_users SET Name = ?Name, UserType = ?UserType" +
                    " WHERE UserID = ?UserID";
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?Name", user.name);
            command.Parameters.AddWithValue("?UserType", user.userType);
            command.Parameters.AddWithValue("?UserID", user.Uid);
            string message = "Do you want to save changes?";
            string caption = "Confirmation";
            MessageBoxResult result = MessageBox.Show(message, caption,
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                DB_Connector.connection.Open();
                command.ExecuteNonQuery();
                DB_Connector.connection.Close();
                updated = true;
            }
            else
                updated = false;
            return updated;
        }

        public static bool userExists(string inName) //checks if the user is int the DB
        {
            string query = "SELECT * FROM t_users WHERE Name = ?Name;";
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
            command.Parameters.AddWithValue("?Name", inName);
            ObservableCollection<User> users = DB_Connector.GetUsers(command);
            if (users.Count == 0)
                return false;
            else
                return true;
        }
    }
}
