﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public partial class AddNewUserWindow : Window
    {
        public event Action<int> outUid;
        public User user;
        public string userName;
        public string userType;
        public int userId;
        public bool closeImmediately;

        public AddNewUserWindow(User newUser, bool inCloseImmetiately = false)
        {
            InitializeComponent();
            user = newUser;
            name_tb.Text = newUser.name;
            userType_cb.SelectedItem = newUser.userType;
            closeImmediately = inCloseImmetiately;
        }

        private void cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save_btn_Click(object sender, RoutedEventArgs e)
        {
            if (User.userExists(name_tb.Text) == false)
            {
                if (Regex.IsMatch(name_tb.Text, @"^[\p{Lu}][\p{L}\x20\-]*('?)[\p{L}\x20\-]*[\p{Ll}]+$") && userType_cb.SelectedItem != null)
                {
                    DB_Connector.getConnection();
                    user.name = name_tb.Text;
                    user.userType = userType_cb.Text;
                    string query = "INSERT INTO t_users (Name, UserType)" +
                        " VALUES (?Name, ?UserType);" +
                        " SELECT LAST_INSERT_ID();";
                    MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                    command.Parameters.AddWithValue("?Name", user.name);
                    command.Parameters.AddWithValue("?UserType", user.userType);
                    DB_Connector.connection.Open();
                    user.Uid = Convert.ToInt32(command.ExecuteScalar());  //Hier funktioniert es!
                    DB_Connector.connection.Close();
                    userName = user.name;
                    userType = user.userType;
                    userId = user.Uid;
                    MessageBox.Show(name_tb.Text + " successfully added to the database.");
                    name_tb.Text = "";
                    userType_cb.SelectedItem = null;
                    Keyboard.Focus(name_tb);
                    if (closeImmediately == true)
                    {
                        this.Close();
                    }
                }
                else
                {
                    name_tb.Text = "";
                    MessageBox.Show("Please, check the name format.");
                }
            }
            else
                MessageBox.Show("This user is already saved in the database.");
        }
        
        private void name_tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (name_tb.Text.Length > 2)
            {
                ObservableCollection<User> users = User.GetUsers(name_tb.Text);
                List<string> userStrings = new List<string>();
                foreach (User u in users)
                {
                    userStrings.Add(u.name);
                }
                suggest_lb.ItemsSource = userStrings;
                popUp.IsOpen = true;
                suggest_lb.Visibility = Visibility.Visible;
            }
            else
            {
                suggest_lb.ItemsSource = null;
                popUp.IsOpen = false;
                suggest_lb.Visibility = Visibility.Collapsed;
            }
        }
    }
}
