﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    class Glob
    {
        public static List<Location> AllLocations { get; set; }

        public static string tempUserType { get; set; }

        public static void enableButton(Button button)
        {
            button.IsEnabled = true;
            button.Foreground = new SolidColorBrush(Colors.Black);
        }

        public static void disableButton(Button button)
        {
            button.IsEnabled = false;
            button.Foreground = new SolidColorBrush(Colors.Gray);
            
        }
    }
}
