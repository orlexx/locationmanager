﻿#pragma checksum "..\..\AllUsersWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "184E9515C49EEF32F64D5C16FBE057E69A1D828A"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using LocationManager;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LocationManager {
    
    
    /// <summary>
    /// AllUsersWindow
    /// </summary>
    public partial class AllUsersWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 18 "..\..\AllUsersWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox search_tb;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\AllUsersWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid usersGrid;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\AllUsersWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridComboBoxColumn userType_cb;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\AllUsersWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button delete_b;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LocationManager;component/alluserswindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AllUsersWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.search_tb = ((System.Windows.Controls.TextBox)(target));
            
            #line 21 "..\..\AllUsersWindow.xaml"
            this.search_tb.GotFocus += new System.Windows.RoutedEventHandler(this.search_tb_GotFocus);
            
            #line default
            #line hidden
            
            #line 21 "..\..\AllUsersWindow.xaml"
            this.search_tb.LostFocus += new System.Windows.RoutedEventHandler(this.search_tb_LostFocus);
            
            #line default
            #line hidden
            
            #line 22 "..\..\AllUsersWindow.xaml"
            this.search_tb.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.search_tb_TextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.usersGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 27 "..\..\AllUsersWindow.xaml"
            this.usersGrid.RowEditEnding += new System.EventHandler<System.Windows.Controls.DataGridRowEditEndingEventArgs>(this.usersGrid_RowEditEnding);
            
            #line default
            #line hidden
            
            #line 32 "..\..\AllUsersWindow.xaml"
            this.usersGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.usersGrid_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.userType_cb = ((System.Windows.Controls.DataGridComboBoxColumn)(target));
            return;
            case 5:
            this.delete_b = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\AllUsersWindow.xaml"
            this.delete_b.Click += new System.Windows.RoutedEventHandler(this.delete_b_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 4:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.Controls.Primitives.Selector.SelectionChangedEvent;
            
            #line 40 "..\..\AllUsersWindow.xaml"
            eventSetter.Handler = new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

