﻿#pragma checksum "..\..\SearchEventWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2D7C0023BC30E2E197A3B9F2948DA56149E521E5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using LocationManager;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LocationManager {
    
    
    /// <summary>
    /// SearchEventWindow
    /// </summary>
    public partial class SearchEventWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\SearchEventWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker date_dp;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\SearchEventWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox search_tb;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\SearchEventWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid eventsGrid;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\SearchEventWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button delete_b;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\SearchEventWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button edit_b;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LocationManager;component/searcheventwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SearchEventWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.date_dp = ((System.Windows.Controls.DatePicker)(target));
            
            #line 17 "..\..\SearchEventWindow.xaml"
            this.date_dp.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.date_dp_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.search_tb = ((System.Windows.Controls.TextBox)(target));
            
            #line 25 "..\..\SearchEventWindow.xaml"
            this.search_tb.GotFocus += new System.Windows.RoutedEventHandler(this.search_tb_GotFocus);
            
            #line default
            #line hidden
            
            #line 26 "..\..\SearchEventWindow.xaml"
            this.search_tb.LostFocus += new System.Windows.RoutedEventHandler(this.search_tb_LostFocus);
            
            #line default
            #line hidden
            
            #line 27 "..\..\SearchEventWindow.xaml"
            this.search_tb.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.search_tb_TextChanged);
            
            #line default
            #line hidden
            
            #line 31 "..\..\SearchEventWindow.xaml"
            this.search_tb.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.search_tb_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.eventsGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 39 "..\..\SearchEventWindow.xaml"
            this.eventsGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.eventsGrid_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 40 "..\..\SearchEventWindow.xaml"
            this.eventsGrid.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.eventsGrid_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.delete_b = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\SearchEventWindow.xaml"
            this.delete_b.Click += new System.Windows.RoutedEventHandler(this.delete_b_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.edit_b = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\SearchEventWindow.xaml"
            this.edit_b.Click += new System.Windows.RoutedEventHandler(this.edit_b_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

