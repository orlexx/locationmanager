﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;


namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public class DB_Connector
    {
        private const String server = "127.0.0.1";
        private const String database = "locationsdb";
        private const String uid = "root";
        private const String pw = "toor";
        public static MySqlConnection connection;

        public static MySqlConnection getConnection()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();

            builder.Server = server;
            builder.UserID = uid;
            builder.Password = pw;
            builder.Database = database;

            String connectionString = builder.ToString();

            builder = null;

            connection = new MySqlConnection(connectionString);
            return connection;
        }

        public static ObservableCollection<User> GetUsers(MySqlCommand command)
        {
            ObservableCollection<User> users = new ObservableCollection<User>();
            MySqlConnection connection = getConnection();
            command.Connection = connection;
            using (connection)
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = (int)reader["UserId"];
                    string name = reader["Name"].ToString();
                    string userType = reader["UserType"].ToString();
                    User u = new User(id, name, userType);
                    users.Add(u);
                }
            }
            return users;
        }
    }
}
