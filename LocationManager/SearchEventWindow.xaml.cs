﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace LocationManager
{
    // Author: Alexey Orlov, 2018.
    public partial class SearchEventWindow : Window
    {
        List<Event> foundEvents = new List<Event>();

        public SearchEventWindow()
        {
            InitializeComponent();
            Glob.disableButton(delete_b);
            Glob.disableButton(edit_b);
            search_tb_ToDefault();
            fillEventsGrid();
        }

        private void search_tb_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search_tb.Text == "Enter event or instructor name here")
            {
                search_tb.Text = "";
                search_tb.FontStyle = FontStyles.Normal;
                search_tb.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void search_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (search_tb.Text == "")
                search_tb_ToDefault();            
        }

        private void search_tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (search_tb.Text.Length > 2 && search_tb.Text != "Enter event or instructor name here")
            {
                foundEvents.Clear();
                string queryByEvent = "SELECT * FROM t_events WHERE Name LIKE (CONCAT('%', ?Query, '%')); ";
                string queryByInstructor = "SELECT * FROM t_events e INNER JOIN t_users u ON (e.InstructorID = u.UserID) " +
                        "WHERE u.Name LIKE (CONCAT('%', ?Query, '%'));";
                foundEvents.AddRange(searchEvents(queryByEvent));
                foundEvents.AddRange(searchEvents(queryByInstructor));

                using (DB_Connector.connection) //this closes the connection after use.
                {
                    DB_Connector.connection.Open();
                    foreach (Event ev in foundEvents)
                    {
                        string query = "SELECT locType FROM t_locations WHERE locNr = ?locNr;";
                        MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("?locNr", ev.EventLocation.LocationNr);
                        MySqlDataReader reader = command.ExecuteReader(); //read data from db.
                        while(reader.Read())
                        {
                            ev.EventLocation.LocationType = reader["locType"].ToString();
                        }
                        reader.Close(); //has to be closed to prevent errors.
                        query = "SELECT Name, UserType FROM t_users WHERE UserID = ?UserID;";
                        command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("?UserID", ev.Instructor.Uid);
                        reader = command.ExecuteReader();
                        while(reader.Read())
                        {
                            ev.Instructor.name = reader["Name"].ToString();
                            ev.Instructor.userType = reader["UserType"].ToString();
                        }
                        reader.Close();
                    }
                } // end using connection
                eventsGrid.ItemsSource = null;
                eventsGrid.ItemsSource = foundEvents;
            } //end if
            else
            {
                fillEventsGrid();
            }
        }

        private void fillEventsGrid()
        {
            List<Event> events = new List<Event>();
            string query = "SELECT * FROM t_events";
            MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);

            if (DB_Connector.connection.State != System.Data.ConnectionState.Open)
                DB_Connector.connection.Open();
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Event myEvent = new Event(
                        Convert.ToInt32(reader["EventID"]),
                        DateTime.Parse(reader["start_datetime"].ToString()),
                        DateTime.Parse(reader["end_datetime"].ToString()),
                        new User(Convert.ToInt32(reader["InstructorID"])),
                        new Location(Convert.ToInt32(reader["LocationID"])),
                        reader["Name"].ToString(),
                        reader["Description"].ToString()
                        );
                    events.Add(myEvent);
                }
            }
            DB_Connector.connection.Close();
            eventsGrid.ItemsSource = null;
            eventsGrid.ItemsSource = events;
        }

        private List<Event> searchEvents(string query)
        {
            List<Event> events = new List<Event>();
            using (DB_Connector.connection)
            {
                MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                command.Parameters.AddWithValue("?Query", search_tb.Text);
                DB_Connector.connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Event myEvent = new Event(
                            Convert.ToInt32(reader["EventID"]),
                            DateTime.Parse(reader["start_datetime"].ToString()),
                            DateTime.Parse(reader["end_datetime"].ToString()),
                            new User(Convert.ToInt32(reader["InstructorID"])),
                            new Location(Convert.ToInt32(reader["LocationID"])),
                            reader["Name"].ToString(),
                            reader["Description"].ToString()
                            );
                        events.Add(myEvent);
                    }
                }
                reader.Close();
            }
            return events;
        }

        private void delete_b_Click(object sender, RoutedEventArgs e)
        {
            if (eventsGrid.SelectedItem != null) // if a certain event is selected
            {
                Event selectedEvent = (Event)eventsGrid.SelectedItem;
                string message = String.Format("Do you want to delete event {0}?", selectedEvent.EventName);
                string caption = "Question.";
                MessageBoxResult result = MessageBox.Show(message, caption, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    string query = "DELETE FROM t_events WHERE EventID = ?EventID;";
                    using (DB_Connector.connection)
                    {
                        DB_Connector.connection.Open();
                        MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("?EventID", selectedEvent.EventID);
                        command.ExecuteNonQuery();
                    }
                    fillEventsGrid();// removes event from Grid
                }                    
            }
        }

        private void edit_b_Click(object sender, RoutedEventArgs e)
        {
            Event selectedEvent = (Event)eventsGrid.SelectedItem;
            EventDetailsWindow edw = new EventDetailsWindow(selectedEvent);
            edw.Show();
        }

        private void search_tb_ToDefault()
        {
            search_tb.Text = "Enter event or instructor name here";
            search_tb.FontStyle = FontStyles.Italic;
            search_tb.Foreground = new SolidColorBrush(Colors.Gray);
        }

        private void eventsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (eventsGrid.SelectedItem != null)
            {
                Glob.enableButton(edit_b);
                Glob.enableButton(delete_b);
                edit_b.FontWeight = FontWeights.Bold;
            }
            else
            {
                Glob.disableButton(edit_b);
                Glob.disableButton(delete_b);
            }
        }
        
        private void date_dp_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (date_dp.SelectedDate != null)
            {
                string query = "SELECT * FROM t_events WHERE DAY(start_datetime) = ?day AND " +
                    "MONTH(start_datetime) = ?month AND YEAR(start_datetime) = ?year;";
                foundEvents.Clear();
                using (DB_Connector.connection)
                {
                    DB_Connector.connection.Open();
                    MySqlCommand command = new MySqlCommand(query, DB_Connector.connection);
                    command.Parameters.AddWithValue("?day", date_dp.SelectedDate.Value.Day);
                    command.Parameters.AddWithValue("?month", date_dp.SelectedDate.Value.Month);
                    command.Parameters.AddWithValue("?year", date_dp.SelectedDate.Value.Year);
                    MySqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            foundEvents.Add(new Event(Convert.ToInt32(reader["EventID"]),
                            DateTime.Parse(reader["start_datetime"].ToString()),
                            DateTime.Parse(reader["end_datetime"].ToString()),
                            new User(Convert.ToInt32(reader["InstructorID"])),
                            new Location(Convert.ToInt32(reader["LocationID"])),
                            reader["Name"].ToString(),
                            reader["Description"].ToString()));
                        }
                    }
                    reader.Close();
                    foreach (Event ev in foundEvents)
                    {
                        query = "SELECT locType FROM t_locations WHERE locNr = ?locNr;";
                        command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("?locNr", ev.EventLocation.LocationNr);
                        reader = command.ExecuteReader(); //read data from db.
                        while (reader.Read())
                        {
                            ev.EventLocation.LocationType = reader["locType"].ToString();
                        }
                        reader.Close(); //has to be closed to prevent errors.
                        query = "SELECT Name, UserType FROM t_users WHERE UserID = ?UserID;";
                        command = new MySqlCommand(query, DB_Connector.connection);
                        command.Parameters.AddWithValue("?UserID", ev.Instructor.Uid);
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            ev.Instructor.name = reader["Name"].ToString();
                            ev.Instructor.userType = reader["UserType"].ToString();
                        }
                        reader.Close();
                    }
                }
                eventsGrid.ItemsSource = foundEvents;                        
            }
        }

        private void search_tb_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Down && eventsGrid.Items.Count > 0)
            {
                Keyboard.ClearFocus();
                eventsGrid.SelectedIndex = 0;
                edit_b.FontWeight = FontWeights.Bold;
                Glob.enableButton(edit_b);
                Glob.enableButton(delete_b);
                edit_b.IsDefault = true;
                eventsGrid.UpdateLayout();
                var dgr = (DataGridRow)eventsGrid.ItemContainerGenerator.ContainerFromItem(eventsGrid.Items[0]);
                FocusManager.SetIsFocusScope(dgr, true);
                FocusManager.SetFocusedElement(dgr, dgr);
            }
        }

        private void eventsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(eventsGrid.Items.Count > 0)
            {                
                if (e.Key == Key.Enter)
                {
                    Event selectedEvent = (Event)eventsGrid.SelectedItem;
                    EventDetailsWindow edw = new EventDetailsWindow(selectedEvent);
                    edw.Show();
                }
                if(e.Key == Key.Down)
                {
                    if (eventsGrid.SelectedIndex < eventsGrid.Items.Count - 1)
                    {
                        eventsGrid.SelectedIndex++;                        
                    }
                    else
                    {
                        eventsGrid.SelectedIndex = 0;
                    }
                }
                if (e.Key == Key.Up)
                {
                    if (eventsGrid.SelectedIndex == 0)
                        eventsGrid.SelectedIndex = eventsGrid.Items.Count - 1;
                    else
                        eventsGrid.SelectedIndex--;
                }
                eventsGrid.UpdateLayout();
                DataGridRow dgr = (DataGridRow)eventsGrid.ItemContainerGenerator.ContainerFromItem(eventsGrid.SelectedItem);
                FocusManager.SetIsFocusScope(dgr, true);
                FocusManager.SetFocusedElement(dgr, dgr);
            }            
        }        
    }
}
